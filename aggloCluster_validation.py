

### Import Necessary Packages ###

import ROOT 
import numpy as np
from root_numpy import root2array, tree2array, rec2array
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale, normalize
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import least_squares, minimize

### Choose User Inputs ###

categoryLabels = [  'is_passed_ttHhad' ,
                    'is_passed_ttHhad_4jet1bjet' ,
                    'is_passed_ttHhad_4jet2bjet' ,
                    'is_passed_ttHhad_5jet1bjet' ,
                    'is_passed_ttHhad_5jet2bjet' ,
                    'is_passed_ttHhad_6jet1bjet' ,
                    'is_passed_ttHhad_6jet2bjet' ,
                    'is_passed_tHlep_0fwd' ,
                    'is_passed_tHlep_1fwd' ,
                    'is_passed_ttHlep' ,
                    'is_passed_ttHhad_bdt1' ,
                    'is_passed_ttHhad_bdt2' ,
                    'is_passed_ttHhad_bdt3' ,
                    'is_passed_ttHhad_bdt4' ,
                    'is_passed_tHhad_bdt' ,
                    'is_passed_ttHlep_bdt1' ,
                    'is_passed_ttHlep_bdt2' ,
                    'is_passed_tHlep_bdt1' ,
                    'is_passed_tHlep_bdt2' ,
                    'is_passed_ttHhad_bdt_presel' ,
                    'is_passed_ttHlep_bdt_presel' ]

# CATEGORIES: 
# 'is_passed_ttHhad' = 0
# 'is_passed_ttHhad_4jet1bjet' = 1
# 'is_passed_ttHhad_4jet2bjet' = 2 
# 'is_passed_ttHhad_5jet1bjet' = 3
# 'is_passed_ttHhad_5jet2bjet' = 4
# 'is_passed_ttHhad_6jet1bjet' = 5
# 'is_passed_ttHhad_6jet2bjet' = 6
# 'is_passed_tHlep_0fwd' = 7
# 'is_passed_tHlep_1fwd' = 8
# 'is_passed_ttHlep' = 9
# 'is_passed_ttHhad_bdt1' = 10 
# 'is_passed_ttHhad_bdt2' = 11
# 'is_passed_ttHhad_bdt3' = 12
# 'is_passed_ttHhad_bdt4' = 13
# 'is_passed_tHhad_bdt' = 14
# 'is_passed_ttHlep_bdt1' = 15
# 'is_passed_ttHlep_bdt2' = 16
# 'is_passed_tHlep_bdt1' = 17
# 'is_passed_tHlep_bdt2' = 18
# 'is_passed_ttHhad_bdt_presel' = 19 
# 'is_passed_ttHlep_bdt_presel' = 20 

# weight_total = 21 
# bdt_category_weight = 22 

# User-Defined Variables 
nClusters = 13
nNeighbors = 5


### Import the Data File and MC Files ###

inputDir = 'inputTrees/'

in_Data_FileNames = [ "data2016" ] 
in_MC_FileNames = [ "Sherpa_2DP20_myy_100_160" , # Sherpa yy MC 
					#"Pythia8_gammajet_DP70_280" , # Pythia y+j MC 
					"Sherpa_SinglePhoton_Pt70_280" , # Sherpa y+j MC 
                    #"Sherpa3jets" , # Sherpa y+3jet MC 
                    #"MGPy8_ttgamgam_allhad" ,
                    #"MGPy8_ttgamgam_noallhad" ,
                    #"MGPy8_ttgamma_allhad" ,
                    #"MGPy8_ttgamma_noallhad" ,
                    "MGPy8_ttgamma_noallhad_big" ,
                    "PowhegPy8_NNLOPS_ggH125" ] # ggH 
#                     "PowhegPy8_NNPDF30_VBFH125" , # VBF 
#                     "PowhegPy8_ZH125J" , # ZH (VH) 
#                     "PowhegPy8_WmH125J" , # W-H (VH) 
#                     "PowhegPy8_WpH125J" , # W+H (VH) 
#                     "aMCnloPy8_bbH125_yb2" , # bbH 
#                     "aMCnloPy8_bbH125_ybyt" ] # bbH 
#                     "MGPy8_tHjb125_yt_plus1" , # tH 
#                     "aMCnloHwpp_tWH125_yt_plus1" , # tH 
#                     "aMCnloPy8_ttH125" ] # ttH 

# Use a pseudo-data set made of MC events of a particular composition 
in_mcFiles = []
for fileName in in_MC_FileNames: in_mcFiles.append( ROOT.TFile.Open( inputDir+fileName+'.root' ) )

# Get the input Data files 
in_dataFiles = []
for fileName in in_Data_FileNames: in_dataFiles.append( ROOT.TFile.Open( inputDir+fileName+'.root' ) )

# Setting stopping values for the pseudo-data/mc set in order to make sure sets are independent 
stops = [ 7000 , 30000 , 25000 , 80000 ]#, 400 , 50 ]

### Get the Trees ###

inTreeName = 'ttHyyTree'

in_MC_Trees = []
in_PseudoData_Trees = []
for file in in_mcFiles: 
	in_MC_Trees.append( file.Get( inTreeName ) )
	in_PseudoData_Trees.append( file.Get( inTreeName ) )

in_Data_Trees = []
for file in in_dataFiles: 
	in_Data_Trees.append( file.Get( inTreeName ) )

### Convert Data to Numpy NDArray ###

branches2read = [ 'weight_total' , 'bdt_category_weight' ,
                  'DRmin_y_j' , 'Deta_j_j' , 'cosTS_yy' , 'DR_y_y' , 'm_alljet_30' , 'met' , #'m_top_mass' ,
                  'n_jets25_central' , 'n_bjets25_70' , 'n_jets25_fwd' ,
                  #'n_el' , 'n_mu' , 'm_ll' , 'DR_ll' , 'pt_ll' , 'pTlepMET' ,   # These are for leptonic cats
                  'n_lep' , 'mT' , #'HT_30' , 'pTlepMET' ,
                  'y1_pt/m_yy' , 'y2_pt/m_yy' , 'y1_E/m_yy' , 'y2_E/m_yy' , 
                  'y1_eta' , 'y2_eta' , 'abs(y1_phi-y2_phi)' ]
jet_branches2read = [ 'jet_pt[0]' , 'jet_pt[1]' , 'jet_pt[2]' , 'jet_pt[3]' , 'jet_pt[4]' , 'jet_pt[5]' ,
					  'jet_PCbtag[0]' , 'jet_PCbtag[1]' , 'jet_PCbtag[2]' , 'jet_PCbtag[3]' , 'jet_PCbtag[4]' , 'jet_PCbtag[5]' ] 
varItr = 3 

selection2apply = '(is_passed_presel==1&&m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&n_jets25>=1)' #&&y1_is_isolated&&y2_is_isolated&&y1_is_tight&&y2_is_tight)'    

allBranches = branches2read + jet_branches2read

### Pseudo Data (MC events) ###

pseudoData_StrucArrays = []
pseudoData_jetInfoArrays = [] 
pseudoData_ArraysHolder = []  # Some MC arrays won't have enough events passing selection
for i,tree in enumerate(in_PseudoData_Trees): 
    pseudoData_StrucArrays.append( tree2array( tree ,
                            branches = branches2read,
                            selection = selection2apply, 
                            start=0, stop=stops[i], step=1) )
                            #start=0, step=2) )
    pseudoData_jetInfoArrays.append( tree2array( tree ,
                            branches = jet_branches2read,
                            selection = selection2apply, 
                            start=0, stop=stops[i], step=1) ) 
                            #start=0, step=2) ) 
    pseudoData_holdJetInfoArray = np.zeros( (len(pseudoData_jetInfoArrays[-1]) , len(jet_branches2read) ) )
    for row in range(len(pseudoData_jetInfoArrays[-1])):
    	for col in range(len(jet_branches2read)):
    		if ( pseudoData_jetInfoArrays[-1][row][col] ): pseudoData_holdJetInfoArray[row][col] = pseudoData_jetInfoArrays[-1][row][col]
    pseudoData_ArraysHolder.append( np.column_stack( [ rec2array( pseudoData_StrucArrays[-1] ) , np.array(pseudoData_holdJetInfoArray) ] ) )


### Monte Carlo ### 

mc_StrucArrays = []
mc_jetInfoArrays = [] 
mc_ArraysHolder = []  # Some MC arrays won't have enough events passing selection
for i,tree in enumerate(in_MC_Trees): 
    mc_StrucArrays.append( tree2array( tree ,
                            branches = branches2read,
                            selection = selection2apply, 
                            start=stops[i], step=1) ) 
                            #start=1, step=2) ) 
    mc_jetInfoArrays.append( tree2array( tree ,
                            branches = jet_branches2read,
                            selection = selection2apply, 
                            start=stops[i], step=1) )
                            #start=1, step=2) ) 
    mc_holdJetInfoArray = np.zeros( (len(mc_jetInfoArrays[-1]) , len(jet_branches2read) ) )
    for row in range(len(mc_jetInfoArrays[-1])):
    	for col in range(len(jet_branches2read)):
    		if ( mc_jetInfoArrays[-1][row][col] ): mc_holdJetInfoArray[row][col] = mc_jetInfoArrays[-1][row][col]
    mc_ArraysHolder.append( np.column_stack( [ rec2array( mc_StrucArrays[-1] ) , np.array(mc_holdJetInfoArray) ] ) )
   
### Real Data ### 

selection2apply = '(is_passed_presel==1&&m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&n_jets25>=1)'    

data_StrucArrays = []
data_jetInfoArrays = [] 
data_ArraysHolder = []  # Some MC arrays won't have enough events passing selection
for i,tree in enumerate(in_Data_Trees): 
    data_StrucArrays.append( tree2array( tree ,
                            branches = branches2read,
                            selection = selection2apply, 
                            start=0, stop=100000, step=3) ) 
    data_jetInfoArrays.append( tree2array( tree ,
                            branches = jet_branches2read,
                            selection = selection2apply, 
                            start=0, stop=100000, step=3) ) 
    data_holdJetInfoArray = np.zeros( (len(data_jetInfoArrays[-1]) , len(jet_branches2read) ) )
    for row in range(len(data_jetInfoArrays[-1])):
    	for col in range(len(jet_branches2read)):
    		if ( data_jetInfoArrays[-1][row][col] ): data_holdJetInfoArray[row][col] = data_jetInfoArrays[-1][row][col]
    data_ArraysHolder.append( np.column_stack( [ rec2array( data_StrucArrays[-1] ) , np.array(data_holdJetInfoArray) ] ) )
   
print( "Note: using " , len(data_ArraysHolder[0]) , " data events." ) 

### Normalize the Columns in the Arrays and Store the (Unscaled) Weights ### 

# Start with Pseudo-Data
pseudoData_Arrays = []   # Same as data_ArraysHolder, but doesn't include the categories with not enough events 
scaled_PseudoDataArrays = []
weights_PseudoDataArrays = [] 

for i,array in enumerate(pseudoData_ArraysHolder): 
    if( len( array ) > nClusters ): 
        scaled_PseudoDataArrays.append( normalize( array[:,varItr:] ) )
        weights_PseudoDataArrays.append( np.multiply( np.multiply( array[:,0] , array[:,1]) , 36.1 ) )
        pseudoData_Arrays.append( array[:,varItr:] )

scaled_PseudoDataArray = np.vstack( scaled_PseudoDataArrays ) 

# Next do the remaining MC 
mc_Arrays = []   # Same as mc_ArraysHolder, but doesn't include the categories with not enough events 
scaled_mcArrays = []
weights_mcArrays = [] 
labels = []

for i,array in enumerate(mc_ArraysHolder): 
    if( len( pseudoData_ArraysHolder[i] ) > nClusters ): 
        scaled_mcArrays.append( normalize( array[:,varItr:] ) )
        weights_mcArrays.append( np.multiply( np.multiply( array[:,0] , array[:,1]) , 36.1 ) )
        mc_Arrays.append( array[:,varItr:] )
        labels.append( in_MC_FileNames[i] )

labels.append( "Pseudo-Data" )

# Lastly, get the Real Data
data_Arrays = []   # Same as data_ArraysHolder, but doesn't include the categories with not enough events 
scaled_DataArrays = []

for i,array in enumerate(data_ArraysHolder): 
    if( len( array ) > nClusters ): 
        scaled_DataArrays.append( normalize( array[:,varItr:] ) )
        data_Arrays.append( array[:,varItr:] )
        #labels.append( in_Data_FileNames[i] )
scaled_DataArray = np.vstack( scaled_DataArrays ) 


### Apply PCA on Data ### 

print( "Obtaining PCA Axes on Real Data" )

dataPCA = PCA(n_components=2)
dataPCA.fit( scaled_DataArray )
reduced_Data = dataPCA.transform( scaled_DataArray )

### Apply PCA on Pseudo-Data ### 

print( "Applying PCA from Data to Pseudo-Data" )

reduced_pseudoDataArrays = []
for array in scaled_PseudoDataArrays: 
    reduced_pseudoDataArrays.append( dataPCA.transform( array ) )

reduced_PseudoData = dataPCA.transform( scaled_PseudoDataArray )

### Apply PCA From Data To MC ### 

print( "Applying PCA from Data to MC" )

reduced_mcArrays = []
for array in scaled_mcArrays: 
    reduced_mcArrays.append( dataPCA.transform( array ) )


### Print Info on the PCA Components ###

print( "PCA Components: " )
print( "   --- Axis #1 --- " )
indeces = np.argpartition( dataPCA.components_[0], -5 )[-5:]
for i in range(5): 
    print( "   " , allBranches[varItr+indeces[i]] , " - " , dataPCA.components_[0][indeces[i]] )

print( "   --- Axis #2 --- " )
indeces = np.argpartition( dataPCA.components_[1], -5 )[-5:]
for i in range(5): 
    print( "   " , allBranches[varItr+indeces[i]] , " - " , dataPCA.components_[1][indeces[i]] )


### Calculate the Optimal Number of Clusters (Optional) ### 

if( nClusters == -1 ): 
	print( "Obtaining the optimal number of clusters" )
	testVariances = [] 
	for nTest in range(5,40): 
		clusterNTest = AgglomerativeClustering( n_clusters=nTest )
		clusterNTest.fit( scaled_DataArray )
		holdVariances = []
		for icls in range(nTest):
			holdVariances.append( scaled_DataArray[ clusterNTest.labels_==icls ] )
		testVariances.append( [ nTest , np.sum( np.sqrt( holdVariances[0]*holdVariances[0] ) )/nTest ] )
	testVariances = np.array( testVariances )
	plt.figure()
	plt.scatter( testVariances[:,0] , testVariances[:,1] ) 
	plt.show() 

### Apply Clustering ### 

print( "Clustering Data" )

clusterData = AgglomerativeClustering( n_clusters=nClusters )
#clusterData = AgglomerativeClustering( linkage='average' , n_clusters=nClusters , affinity='cosine' )
#clusterData.fit( data_Array )
clusterData.fit( scaled_DataArray )

print( "Training NNeighbors Algorithm on Data" )
### Use Nearest Neighbors Classifier to Predict Clusters of MC and Pseudo-Data Events ### 
neighbors = KNeighborsClassifier(weights='distance',n_neighbors=nNeighbors)
neighbors.fit( scaled_DataArray , clusterData.labels_ )

print( "Classifying Pseudo-Data into Clusters" )
clusterPseudoData = neighbors.predict( scaled_PseudoDataArray )
clusters_PseudoData = []
for i,array in enumerate( scaled_PseudoDataArrays ): 
    clusters_PseudoData.append( neighbors.predict( array ) )
allPseudoDataClusters = np.hstack( clusters_PseudoData )

print( "Classifying MC into Clusters" )
clusterMCs = [] 
for i,array in enumerate( scaled_mcArrays ): 
    clusterMCs.append( neighbors.predict( array ) )


### Plot and the Data and MC Samples ### 

print( "Making Plots" )

markerList = [ 'o' , 'd' , 'D' , '+' , 's' , 'v' , '^' , '<' , 'D' , 'd' , 'o' , 'o' ,'o' ]
nColors = len( reduced_mcArrays )
colorList = plt.cm.Reds(np.linspace(0, 1, nColors+1))

nSamples = len(labels) 

plt.figure(num=1,figsize=[7,7])
plt.clf()

#Get the best axes ranges 
xMax = -9999 
xMin = 9999
yMax = -9999 
yMin = 9999 

xMin = np.min( reduced_mcArrays[0][:,1] ) 

for array in reduced_mcArrays: 
    if( xMin > np.min( reduced_mcArrays[0][:,0] ) ): xMin = np.min( reduced_mcArrays[0][:,0] )
    if( xMax < np.max( reduced_mcArrays[0][:,0] ) ): xMax = np.max( reduced_mcArrays[0][:,0] )
    if( yMin > np.min( reduced_mcArrays[0][:,1] ) ): yMin = np.min( reduced_mcArrays[0][:,1] )
    if( yMax < np.max( reduced_mcArrays[0][:,1] ) ): yMax = np.max( reduced_mcArrays[0][:,1] )

if( xMin > np.min( reduced_PseudoData[:,0] ) ): xMin = np.min( reduced_PseudoData[:,0] )
if( xMax < np.max( reduced_PseudoData[:,0] ) ): xMax = np.max( reduced_PseudoData[:,0] )
if( yMin > np.min( reduced_PseudoData[:,1] ) ): yMin = np.min( reduced_PseudoData[:,1] )
if( yMax < np.max( reduced_PseudoData[:,1] ) ): yMax = np.max( reduced_PseudoData[:,1] )

if( xMin > np.min( reduced_Data[:,0] ) ): xMin = np.min( reduced_Data[:,0] )
if( xMax < np.max( reduced_Data[:,0] ) ): xMax = np.max( reduced_Data[:,0] )
if( yMin > np.min( reduced_Data[:,1] ) ): yMin = np.min( reduced_Data[:,1] )
if( yMax < np.max( reduced_Data[:,1] ) ): yMax = np.max( reduced_Data[:,1] )

xMin = 1.2*xMin
xMax = 1.2*xMax
yMin = 1.2*yMin
yMax = 1.2*yMax

# MC Samples
plt.figure(num=1,figsize=[7,7])
for i,array in enumerate(reduced_mcArrays): 
    plt.scatter(   array[:,0] , 
                   array[:,1] , 
                   c = clusterMCs[i] ,
                   marker = markerList[i] )

plt.title('Clustering of MC Events in PCA Space')
plt.ylabel('PCA Axis #1')
plt.xlabel('PCA Axis #2')
plt.xlim( [xMin, xMax] )
plt.ylim( [yMin, yMax] )
plt.legend(labels[:-2])
plt.xticks(())
plt.yticks(())
plt.savefig("pcaMC_4MCsamples.png")
#plt.show()

# Pseudo-Data 
plt.figure(num=2,figsize=[7,7])
for i,array in enumerate(reduced_pseudoDataArrays): 
    plt.scatter(   array[:,0] , 
                   array[:,1] , 
                   c = clusters_PseudoData[i] ,
                   marker = markerList[i] )
              
plt.title('Clustering of Pseudo-Data in PCA Space')
plt.ylabel('PCA Axis #1')
plt.xlabel('PCA Axis #2')
plt.xlim( [xMin, xMax] )
plt.ylim( [yMin, yMax] )
plt.legend(labels[:-2])
plt.legend(["Pseudo-Data"])
plt.xticks(())
plt.yticks(())
plt.savefig("pcaPseudoData_4MCsamples.png")
#plt.show()

# Real Data 
plt.figure(num=3,figsize=[7,7])
plt.scatter(  reduced_Data[:,0] , 
              reduced_Data[:,1] , 
              c=clusterData.labels_,
              marker = '*')
              
plt.title('Clustering of Real Data in PCA Space')
plt.ylabel('PCA Axis #1')
plt.xlabel('PCA Axis #2')
plt.xlim( [xMin, xMax] )
plt.ylim( [yMin, yMax] )
plt.legend(["2016 Data"])
plt.xticks(())
plt.yticks(())
plt.savefig("pcaData_4MCsamples.png")
#plt.show()

### Visualize the Background Sample Distribution Into Clusters ### 

xbins = []
allSampleFracs = []
cluster_labels = [] 

for n in range(nClusters): 
    sampleFrac = [] 
    for i,array in enumerate( clusterMCs ): 
        sampleFrac.append( np.count_nonzero( array[:]==n ) / len(array) )
    sampleFrac.append( 0.0 )
    sampleFrac.append( np.count_nonzero(allPseudoDataClusters[:] == n) / len(allPseudoDataClusters) )
    sampleFrac.append( np.count_nonzero(clusterData.labels_[:] == n) / len(clusterData.labels_) )
    allSampleFracs.append( sampleFrac )
    xbins.append( [x for x in range(nSamples+2)] )
    cluster_labels.append( "Cluster "+str(n) )


plt.figure(num=4,figsize=[16,7])
bins = [x for x in range(nSamples+3)]
x2_axis = [ (x+0.5) for x in range(nSamples+3) ]
x2_labels = labels[:-1] + [ " " , labels[-1] , "Data" ] 
plt.hist(xbins, bins=bins, stacked=True, weights=allSampleFracs)
plt.title('Event Decomposition Into Cluster')
plt.ylabel('Cluster Distribution')
plt.xlabel('Sample')
plt.xticks(x2_axis, x2_labels ,rotation=20)
plt.legend(cluster_labels)
plt.tight_layout()
plt.savefig("ClusterSampleDists_4MCsamples.pdf")
#plt.show()


### Visualize the Cluster Purities ### 

xbins = []
allClusterFracs = []
x1_labels = [] 

for array in clusterMCs: 
    clusterFrac = [] 
    for n in range( nClusters ): 
        clusterFrac.append( np.count_nonzero( array[:]==n ) )
        x1_labels.append( "Cluster "+str(n) )
    allClusterFracs.append( clusterFrac )
    xbins.append( [x for x in range(nClusters)] )

clusterTotals = np.sum( allClusterFracs, axis=0 )
normClusterFracs = []
for i in range(len(allClusterFracs)):
	normClusterFracs.append( np.divide( allClusterFracs[i] , clusterTotals ) )

plt.figure(num=5,figsize=[16,7])
bins = [x for x in range(nClusters+1)]
x_axis = [ (x+0.5) for x in range(nClusters) ]
legend_labels = labels[:-1] + [ " " , labels[-1] ] 
plt.hist(xbins, bins=bins, stacked=True, weights=normClusterFracs)
plt.title('Cluster Purities')
plt.ylabel('Fraction in Each Cluster')
plt.xlabel('Cluster Number')
plt.xticks(x_axis, x1_labels ,rotation=20)
plt.legend(legend_labels)
plt.tight_layout()
plt.savefig("ClusterPurities_4MCsamples.pdf")
#plt.show()


### Visualize the Sample Distributions Separately ### 

plt.figure(num=5,figsize=[16,7])
bins = [x for x in range(nClusters+1)]
x_axis = [ (x+0.5) for x in range(nClusters) ]
legend_labels = labels[:-1] + [ " " , labels[-1] ] 
plt.hist(xbins, bins=bins, stacked=True, weights=normClusterFracs)
plt.title('Cluster Purities')
plt.ylabel('Fraction in Each Cluster')
plt.xlabel('Cluster Number')
plt.xticks(x_axis, x1_labels ,rotation=20)
plt.legend(legend_labels)
plt.tight_layout()
plt.savefig("ClusterDistributions_4MCsamples.pdf")
#plt.show()


### Perform a Basic Fit to Estimate the Composition ### 

mcClusterFracs = []
pseudoDataClusterFracs = []
 
for i,array in enumerate( clusterMCs ): 
	holdClusterFracs = []
	for n in range(nClusters):
		holdClusterFracs.append( np.count_nonzero( array[:]==n ) / len(array) )
	mcClusterFracs.append( holdClusterFracs ) 
for n in range(nClusters):
	pseudoDataClusterFracs.append( np.count_nonzero(clusterData.labels_ == n) / len(clusterData.labels_) )

# Use Least-Squares
def fractionFunction( x ): 
	return np.dot(x,mcClusterFracs) - pseudoDataClusterFracs 
bounds = ( (0,)*(len(mcClusterFracs)) , (1,)*(len(mcClusterFracs)) )
x0 = np.multiply( np.ones(len(mcClusterFracs)) , 1.0/len(mcClusterFracs) )
llhResults = least_squares( fractionFunction , x0 , bounds=bounds)

# Use Minimize
def fractionFunction( x ): 
	return np.sum( np.abs( np.dot(x,mcClusterFracs) - pseudoDataClusterFracs ) )
#constr = ({'type': 'eq', 'fun': lambda x:  1.0-sum(x)})
constr = ({'type': 'ineq', 'fun': lambda x: 0.05 - abs(1.0 - np.sum(x))} ) #, 
# 		  {'type': 'ineq', 'fun': lambda x: x[0] } , 
# 		  {'type': 'ineq', 'fun': lambda x: x[1] } ,
# 		  {'type': 'ineq', 'fun': lambda x: x[2] } ,
# 		  {'type': 'ineq', 'fun': lambda x: x[3] }	)
bounds = ( ((0,1),)*(len(mcClusterFracs)) )
minResults = minimize( fractionFunction , x0, method='SLSQP', bounds=bounds,constraints=constr)

### Get the True Composition ###

trueResults = [] 
totalNevents = len(scaled_PseudoDataArray)
for i,array in enumerate( pseudoData_Arrays ):
	trueResults.append( len(array) / totalNevents )

error = np.subtract( llhResults.x , trueResults )

### Print Results ### 

print( "----- FOUND COMPOSITION ESTIMATE ----- " )
print( "MC Samples Accepted: " )
print( "\t", labels )
for iSample in range(len(pseudoData_Arrays)): 
	print( "\t# PseudoData events from MC Sample " , iSample , " = " , len(pseudoData_Arrays[iSample]) )
print( "Prediction: " )
print( "\t" , llhResults.x ) 
print( "Truth: " ) 
print( "\t" , trueResults ) 
print( " " ) 
print( "Error: " )
print( "\t" , error )
print( " " ) 
print( " " ) 
print( "DEBUGGING: " ) 
for iCluster in range(nClusters): 
	print( "Cluster #" , iCluster , " - " , np.count_nonzero(clusterData.labels_[:]==iCluster) )
print( "Number of training data events = " , len(data_Arrays[0]) )

plt.show()