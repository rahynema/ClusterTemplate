### Import Necessary Packages ###

import ROOT 
import numpy as np
from root_numpy import root2array, tree2array, rec2array
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
from sklearn.metrics import calinski_harabaz_score, silhouette_score
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors


### Here Begin the Sub-Functions Used ###

def readInputFiles( in_MC_FileNames , in_Data_FileNames , selection2apply , branches2read , jet_branches2read ): 

	inputDir = 'inputTrees/'

	# Get the input MC files 
	in_mcFiles = []
	for fileName in in_MC_FileNames: in_mcFiles.append( ROOT.TFile.Open( inputDir+fileName+'.root' ) )

	# Get the input Data files 
	in_dataFiles = []
	for fileName in in_Data_FileNames: in_dataFiles.append( ROOT.TFile.Open( inputDir+fileName+'.root' ) )

	# Get the Trees 
	inTreeName = 'ttHyyTree'

	in_MC_Trees = []
	for file in in_mcFiles: 
		in_MC_Trees.append( file.Get( inTreeName ) )

	in_Data_Trees = []
	for file in in_dataFiles: 
		in_Data_Trees.append( file.Get( inTreeName ) )

	branches2read = [ 'weight_total' , 'bdt_category_weight' ] + branches2read 
	varItr = 3 
	allBranches = branches2read + jet_branches2read
	nClustersMax = 20 

	# Construct the Monte Carlo Array

	mc_StrucArrays = []
	mc_jetInfoArrays = [] 
	mc_Arrays = [] 
	scaled_mcArrays = []
	weights_mcArrays = [] 
	labels = [] 	# This just holds the names of the MC samples for later plot legends 
	for i,tree in enumerate(in_MC_Trees): 
		mc_StrucArrays.append( tree2array( tree ,
								branches = branches2read,
								selection = selection2apply ) )
		mc_jetInfoArrays.append( tree2array( tree ,
								branches = jet_branches2read,
								selection = selection2apply ) )
		mc_holdJetInfoArray = np.zeros( (len(mc_jetInfoArrays[-1]) , len(jet_branches2read) ) )
		for row in range(len(mc_jetInfoArrays[-1])):
			for col in range(len(jet_branches2read)):
				if ( mc_jetInfoArrays[-1][row][col] ): mc_holdJetInfoArray[row][col] = mc_jetInfoArrays[-1][row][col]
		mc_Arrays.append( np.column_stack( [ rec2array( mc_StrucArrays[-1] ) , np.array(mc_holdJetInfoArray) ] ) )
		if( len(mc_Arrays[-1]) < nClustersMax ): del mc_Arrays[-1]
		else:
			scaled_mcArrays.append( normalize( mc_Arrays[-1][:,varItr:] ) )
			weights_mcArrays.append( np.multiply( np.multiply( mc_Arrays[-1][:,0] , mc_Arrays[-1][:,1]) , 36.1 ) ) 
			labels.append( in_MC_FileNames[i] )
   
	# Construct the Data Array

	data_StrucArrays = []
	data_jetInfoArrays = [] 
	data_Arrays = [] 
	scaled_DataArrays = []
	for i,tree in enumerate(in_Data_Trees): 
		data_StrucArrays.append( tree2array( tree ,
								branches = branches2read,
								selection = selection2apply , stop = 500000) )
		data_jetInfoArrays.append( tree2array( tree ,
								branches = jet_branches2read,
								selection = selection2apply , stop = 500000 ) )
		data_holdJetInfoArray = np.zeros( (len(data_jetInfoArrays[-1]) , len(jet_branches2read) ) )
		for row in range(len(data_jetInfoArrays[-1])):
			for col in range(len(jet_branches2read)):
				if ( data_jetInfoArrays[-1][row][col] ): data_holdJetInfoArray[row][col] = data_jetInfoArrays[-1][row][col]
		data_Arrays.append( np.column_stack( [ rec2array( data_StrucArrays[-1] ) , np.array(data_holdJetInfoArray) ] ) )
		if( len( data_Arrays[-1] ) < nClustersMax ): del data_Arrays[-1]
		else: scaled_DataArrays.append( normalize( data_Arrays[-1][:,varItr:] ) )	

	return data_Arrays, scaled_DataArrays, mc_Arrays, scaled_mcArrays, weights_mcArrays, labels 



def getDataPCA( scaledData ): 

	dataPCA = PCA(n_components=2)
	dataPCA.fit( scaledData )
	reducedData = dataPCA.transform( scaledData )

	return reducedData , dataPCA



def applyDataPCA( scaledArrays , dataPCA ): 

	reducedArrays = []
	for array in scaledArrays: 
		reducedArrays.append( dataPCA.transform( array ) )
		
	return reducedArrays 



def getBestNClusters( scaled_DataArray ): 
	
	testVariances = [] 
	testScores = [] 
	testScores2 = [] 
	
	for nTest in range(2,100): 
		clusterNTest = AgglomerativeClustering( n_clusters=nTest )
		clusterNTest.fit( scaled_DataArray )
		holdVariances = []
		for icls in range(nTest):
			holdVariances.append( scaled_DataArray[ clusterNTest.labels_==icls ] )
		testVariances.append( [ nTest , np.sum( np.sqrt( holdVariances[0]*holdVariances[0] ) )/nTest ] )
		#testScores.append( silhouette_score( scaled_DataArray , clusterNTest.labels_ ) )
		testScores2.append( calinski_harabaz_score( scaled_DataArray , clusterNTest.labels_ ) ) 
	testVariances = np.array( testVariances )
	
#	nClusterFit = np.polyfit( testVariances[:,0], np.log(testVariances[:,1]), 1, w=np.sqrt(testVariances[:,1]) )
# 	curveFit = [ np.e**(nClusterFit[0]*x) * np.e**(nClusterFit[1]) for x in testVariances[:,0] ] 
# 	curveFit_1Deriv = [ np.e**(nClusterFit[0]*x) * np.e**(nClusterFit[1]) * (nClusterFit[0]) for x in testVariances[:,0] ] 
# 	curveFit_2Deriv = [ np.e**(nClusterFit[0]*x) * np.e**(nClusterFit[1]) * (nClusterFit[0])*(nClusterFit[0]) for x in testVariances[:,0] ] 
#	curveFit = np.array( curveFit )
		
	plt.figure()
	#plt.plot( testVariances[:,0] , testScores2[:] , c='red' ) 
	plt.plot( testVariances[:,0] ,testVariances[:,1] , c='red' ) 
	plt.show() 
	
	nBestClusters = 2 #HOLDER 
	
	return nBestClusters 



def applyDataClustering( scaled_DataArray , nClusters , nNeighbors ): 
	
	clusterData = AgglomerativeClustering( n_clusters=nClusters )
	clusterData.fit( scaled_DataArray ) 

	nearestNeighbors = KNeighborsClassifier(weights='distance',n_neighbors=nNeighbors)
	nearestNeighbors.fit( scaled_DataArray , clusterData.labels_ )

	return clusterData , nearestNeighbors 
	


def applyNNeighbors( nearestNeighbors , scaledArrays ): 

	clusteredArrays = []
	for array in scaledArrays: 
		clusteredArrays.append( nearestNeighbors.predict( array ) )
	
	return clusteredArrays 



def getMCErrors( weights , testLabels , nClusters ): 

	nSubSampels = len( weights )
	nTests = len( testLabels )
	
	avgFractionsPerCluster = []
	errFractionsPerCluster = []

	for iSample in range(nSubSampels): 
		holdFractions = [] 
		totalWeight = np.sum( weights[iSample] )
		for iTest in range( nTests ): 
			testFractions = []
			for cluster in range(nClusters): 			
				testFractions.append( np.sum( weights[iSample][testLabels[iTest][iSample]==cluster] ) / totalWeight )
			holdFractions.append( testFractions )
		holdFractions = np.array( holdFractions )
		
		avgFractionsPerCluster.append( np.mean(holdFractions,0) )
		errFractionsPerCluster.append( np.std(holdFractions,0) )

	if( nSubSampels == 1 ): return errFractionsPerCluster[0] 
	else: return errFractionsPerCluster



def getPDErrors( weights , testLabels , nClusters ): 

	nTests = len( testLabels )
	
	avgFractionsPerCluster = []
	errFractionsPerCluster = []

	holdFractions = [] 
	totalWeight = np.sum( weights )
	for iTest in range( nTests ): 
		testFractions = []
		for cluster in range(nClusters): 
			testFractions.append( np.sum( weights[testLabels[iTest][:]==cluster] ) / totalWeight )
		holdFractions.append( testFractions )
	holdFractions = np.array( holdFractions )
	
	avgFractionsPerCluster.append( np.mean(holdFractions,0) )
	errFractionsPerCluster.append( np.std(holdFractions,0) )

	return errFractionsPerCluster[0] 



def getCompositionByCluster( weights , labels , nClusters ): 

	nSubSampels = len( weights )
	fractionPerCluster = []

	for iSample in range(nSubSampels): 
		holdEventsPerBin = []
		totalWeight = np.sum( weights[iSample] )
		for cluster in range(nClusters): 
			holdEventsPerBin.append( np.sum( weights[iSample][labels[iSample][:]==cluster] ) / totalWeight )
		fractionPerCluster.append( holdEventsPerBin ) 	
		
	if( nSubSampels == 1 ): return fractionPerCluster[0] 
	else: return fractionPerCluster



### Below are Plotting Functions ### 


def getNiceAxes( arrays ): 

	xMax = -9999 
	xMin = 9999
	yMax = -9999 
	yMin = 9999 

	for array in arrays: 
		if( xMin > np.min( array[:,0] ) ): xMin = np.min( array[:,0] )
		if( xMax < np.max( array[:,0] ) ): xMax = np.max( array[:,0] )
		if( yMin > np.min( array[:,1] ) ): yMin = np.min( array[:,1] )
		if( yMax < np.max( array[:,1] ) ): yMax = np.max( array[:,1] )

	xMin = 1.2*xMin
	xMax = 1.2*xMax
	yMin = 1.2*yMin
	yMax = 1.2*yMax

	return xMin,xMax,yMin,yMax 



def makeClusterPCAplot_MC( reduced_mcArrays , clusterLabels , whichColors , plotRange , plotName , markerList ): 
	
	xMin = plotRange[0] 
	xMax = plotRange[1] 
	yMin = plotRange[2] 
	yMax = plotRange[3]
	
	plt.figure(figsize=[7,7])
	for i,array in enumerate(reduced_mcArrays): 
		thePlotColors = [ whichColors[i] for i in clusterLabels[i] ]
		plt.scatter(   array[:,0] , 
					   array[:,1] , 
					   c = thePlotColors ,
					   marker = markerList[i] )

	plt.title('Clustering of MC Events in PCA Space')
	plt.ylabel('PCA Axis #1')
	plt.xlabel('PCA Axis #2')
	plt.xlim( [xMin, xMax] )
	plt.ylim( [yMin, yMax] )
	plt.legend(labels)
	plt.xticks(())
	plt.yticks(())
	plt.show()
	
	if( plotName != "" ): plt.savefig(plotName+".png")



def makeClusterPCAplot_Data( reducedArray , clusterLabels , whichColors , plotRange , plotName , isPseudoData ): 
	
	xMin = plotRange[0] 
	xMax = plotRange[1] 
	yMin = plotRange[2] 
	yMax = plotRange[3]
	
	thePlotColors = [ whichColors[i] for i in clusterLabels ]
	
	plt.figure(figsize=[7,7])
	plt.scatter(   reducedArray[:,0] , 
				   reducedArray[:,1] , 
				   c = thePlotColors ,
				   marker = "*" )
	
	if( isPseudoData ): plt.title('Clustering of Pseudo-Data in PCA Space')
	else: plt.title('Clustering of Data in PCA Space')
	plt.ylabel('PCA Axis #1')
	plt.xlabel('PCA Axis #2')
	plt.xlim( [xMin, xMax] )
	plt.ylim( [yMin, yMax] )
	plt.xticks(())
	plt.yticks(())
	plt.show()
	
	if( plotName != "" ): plt.savefig(plotName+".png")



### Here is the Main Function ### 

#def main():  

# Define the input files used 
in_Data_FileNames = [ "data2016" ] 
in_MC_FileNames = [ "Sherpa_2DP20_myy_100_160" , # Sherpa yy MC 
					#"Sherpa_SinglePhoton_Pt70_280" , # Sherpa y+j MC 
					"MGPy8_ttgamma_noallhad_big" ] # tt+y 
					#"PowhegPy8_NNLOPS_ggH125" ] # ggH 

# Define which input variables will be used 
# branches2read = [ 'DRmin_y_j' , 'Deta_j_j' , 'DR_y_y' , 'm_alljet_30' , 'met' , 'DRmin_y_j_2' ,'mT' ,
# 				  #'n_jets25_central' , 'n_bjets25_70' , 'n_jets25_fwd' ,
# 				  #'n_lep' , 'n_el' , 'n_mu' , 
# 				  'y1_pt/m_yy' , 'y2_pt/m_yy' , 'y1_E/m_yy' , 'y2_E/m_yy' , 'pTlepMET' ,
# 				  'y1_topoetcone20/y1_pt' , 'y2_topoetcone20/y2_pt' ,
# 				  'y1_eta' , 'y2_eta' , 'abs(y1_phi-y2_phi)' , 'abs(y1_eta-y2_eta)' , 'abs(y1_pt-y2_pt)' ]
branches2read = [ 'mT' , 'n_jets25_central' , 'n_bjets25_70' ,
				  'y1_pt/m_yy' , 'y2_pt/m_yy' , 'y1_topoetcone20/y1_pt' , 'y2_topoetcone20/y2_pt' ]
jet_branches2read = [ #'jet_pt[0]' , 'jet_pt[1]' , 'jet_pt[2]' , 'jet_pt[3]' , 'jet_pt[4]' , 'jet_pt[5]' ,
				  'jet_PCbtag[0]' , 'jet_PCbtag[1]' , 'jet_PCbtag[2]' , 'jet_PCbtag[3]' , 'jet_PCbtag[4]' , 'jet_PCbtag[5]' ] 
allBranches = branches2read + jet_branches2read

# Define the event selection
selection2apply = '(is_passed_presel==1&&m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&(n_jets25>=2))' #&&y1_is_isolated&&y2_is_isolated&&y1_is_tight&&y2_is_tight)'    

# Generate the data and MC arrays 
data_Arrays, scaled_DataArrays, mc_Arrays, scaled_mcArrays, weights_mcArrays, labels  = readInputFiles( in_MC_FileNames , in_Data_FileNames , selection2apply , branches2read , jet_branches2read ) 
nMCsamples = len(labels) 

# Fix the error of factor of 1000 in tt+yy x-sec 
weights_mcArrays = np.multiply( weights_mcArrays , [1.0 , 1000.0] )

# Combine the data from different input files (right now, just using one) and trim down 
nDataEvents = 25000 
if( len(scaled_DataArrays[0]) < nDataEvents ): nDataEvents = len(scaled_DataArrays[0])
allScaledData = scaled_DataArrays[0][:nDataEvents]   # Since only one input data file is used right now 
allData = data_Arrays[0][:nDataEvents] 

del data_Arrays 
del scaled_DataArrays

# Split the Monte Carlo into "pseudo-data" and reference MC events 
nPDevents = [ 8000 , 4000 ] 
pseudoData = []
scaledPseudoData = [] 
monteCarlo = []
scaledMonteCarlo = [] 
mcWeights = [] 
pseudoDataWeights = []
for iSample in range( nMCsamples ): 
	if( len( mc_Arrays[iSample] ) < nPDevents[iSample] ): 
		print( "WARNING: Not enough events in MC Sample: ", labels[iSample], " - will default to half sample size in pseudo-data. " )
		nPDevents[iSample] = int( np.floor( len( mc_Arrays[iSample] ) / 2.0 ) )
	pseudoData.append( mc_Arrays[iSample][:nPDevents[iSample]] )
	monteCarlo.append( mc_Arrays[iSample][nPDevents[iSample]:] )
	scaledPseudoData.append( scaled_mcArrays[iSample][:nPDevents[iSample]] )
	scaledMonteCarlo.append( scaled_mcArrays[iSample][nPDevents[iSample]:] )
	pseudoDataWeights.append( weights_mcArrays[iSample][:nPDevents[iSample]] )
	mcWeights.append( weights_mcArrays[iSample][nPDevents[iSample]:] )

allScaledPseudoData = np.vstack( scaledPseudoData ) 
allPseudoDataWeights = np.hstack( pseudoDataWeights ) 

# Get the PCA Axes from the data 
allReducedData , dataPCA = getDataPCA( allScaledData )  

# Print Info on the PCA Components 

print( "PCA Components: " )
print( "   --- Axis #1 --- " )
indeces = np.argpartition( dataPCA.components_[0], -5 )[-5:]
for i in range(5): 
    print( "   " , allBranches[indeces[i]] , " - " , dataPCA.components_[0][indeces[i]] )

print( "   --- Axis #2 --- " )
indeces = np.argpartition( dataPCA.components_[1], -5 )[-5:]
for i in range(5): 
    print( "   " , allBranches[indeces[i]] , " - " , dataPCA.components_[1][indeces[i]] )

# Apply the PCA from Data onto MC and PseudoData 

allReducedPseudoData = applyDataPCA( [ allScaledPseudoData ] , dataPCA )
reducedMC = applyDataPCA( scaledMonteCarlo , dataPCA )

# Get the optimal number of clusters 
#nClusters = getBestNClusters( allScaledData ) 

nClusters = 8
nNeighbors = 15

### Will perform cluster labeling on multiple subsets of data to obtain stat-errors! 

nTests = 25
nDataSubsetEvents = np.floor( nDataEvents / ( nTests + 1 ) ) 

pseudoDataTestLabels = []
mcTestLabels = []
dataTestLabels = []

# For testing just using NN error 
dataTest, nearestNeighbors = applyDataClustering( allScaledData , nClusters , nNeighbors ) 

for iTest in range( nTests ): 
	
	# Define Clusters with subset 
# 	scaledDataSubset = allScaledData[ int(nDataSubsetEvents*iTest) : int(nDataSubsetEvents*(iTest+1)) ] 
# 	dataTest, nearestNeighbors = applyDataClustering( scaledDataSubset , nClusters , nNeighbors ) 

	dataTestLabels.append( dataTest.labels_ )
 	
	# Test just doing stat-error calc using the NN algorithm 
	nearestNeighbors = KNeighborsClassifier(weights='distance',n_neighbors=nNeighbors)
	nearestNeighbors.fit( allScaledData[ int(nDataSubsetEvents*iTest) : int(nDataSubsetEvents*(iTest+1)) ] , dataTest.labels_[ int(nDataSubsetEvents*iTest) : int(nDataSubsetEvents*(iTest+1)) ] )
	
	# Get the cluster labels of MC using NNeighbors 
	pseudoDataTestLabels.append( applyNNeighbors( nearestNeighbors , [ allScaledPseudoData ] )[0] )
	mcTestLabels.append( applyNNeighbors( nearestNeighbors , scaledMonteCarlo ) )

# Get the errors based on the multiple NNeighbors tests 

errMCFractionsPerCluster = getMCErrors( mcWeights , mcTestLabels , nClusters ) 
errPseudoDataFractionsPerCluster = getPDErrors( allPseudoDataWeights , pseudoDataTestLabels , nClusters ) 
#errDataFractionsPerCluster = getErrors( [ np.ones(len(dataTestLabels)) ] , dataTestLabels , nClusters )  

### Perform the clustering with all data events to get nominal labels 

dataClustering, nearestNeighbors = applyDataClustering( allScaledData , nClusters , nNeighbors ) 
pseudoDataLabels = applyNNeighbors( nearestNeighbors , [ allScaledPseudoData ] )[0]
mcLabels = applyNNeighbors( nearestNeighbors , scaledMonteCarlo )

### Get the Distribution of Samples in Clusters (the Templates) 

fullMCFractionPerCluster = getCompositionByCluster( mcWeights , mcLabels , nClusters ) 
fullPseudoDataFractionPerCluster = getCompositionByCluster( [ allPseudoDataWeights ] , [ pseudoDataLabels ] , nClusters ) 
fullDataFractionPerCluster = getCompositionByCluster( [ np.ones(len(dataClustering.labels_)) ] , [ dataClustering.labels_ ] , nClusters )  

x_axis = [ (x-0.5) for x in range(nClusters+1) ]
clusterBins = [ x for x in range(nClusters) ]

### Make Plots ### 

cm = plt.get_cmap('gist_rainbow')
cNorm  = colors.Normalize(vmin=0, vmax=nClusters-1)
scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)
myColors = [ scalarMap.to_rgba(i) for i in range(nClusters) ]

sharedAxes = getNiceAxes( reducedMC + [ allReducedData ] + allReducedPseudoData )
markerList = [ 'o' , 'd' , 'D' , '+' , 's' , 'v' , '^' , '<' , 'D' , 'd' , 'o' , 'o' ,'o' ]

makeClusterPCAplot_Data( allReducedData , dataClustering.labels_ , myColors , sharedAxes , "" , 0 )
makeClusterPCAplot_Data( allReducedPseudoData[0] , pseudoDataLabels , myColors , sharedAxes , "" , 1 )
makeClusterPCAplot_MC( reducedMC , mcLabels , myColors , sharedAxes , "" , markerList ) 

### Perform the Optimization with RooFit ###

clusterIDs = np.array( clusterBins )
clusterIDs = ROOT.RooRealVar("clusterIDs","clusterIDs",0,nClusters-1)
argList = ROOT.RooArgList(clusterIDs)

fractionHists = []
fractionRooHists = [] 
fractionRooHistPdfs = []

ArgList_ClusterDistributions = ROOT.RooArgList()
for iSample in range(nMCsamples): 
	histName = "fractions_" + labels[iSample]
	fractionHists.append( ROOT.TH1D(histName,histName,nClusters,-0.5,nClusters-0.5) )
	for iCluster in range(nClusters):
		fractionHists[-1].SetBinContent( fractionHists[-1].FindBin( iCluster ) , fullMCFractionPerCluster[iSample][iCluster] )
		fractionHists[-1].SetBinError( fractionHists[-1].FindBin( iCluster ) , errMCFractionsPerCluster[iSample][iCluster] )
	fractionHists[-1].Sumw2()	
	fractionRooHists.append( ROOT.RooDataHist(histName, histName, argList, fractionHists[-1]) )
	histName = "fractionPdfs_" + labels[iSample]
	fractionRooHistPdfs.append( ROOT.RooHistPdf( histName, histName, ROOT.RooArgSet(clusterIDs), fractionRooHists[-1] )  )
	ArgList_ClusterDistributions.add( fractionRooHistPdfs[-1] )

ArgList_Ratios = ROOT.RooArgList()
ratios = []
for iSample in range(nMCsamples): 	
	ratioName = "Composition_"+labels[iSample]
	ratios.append( ROOT.RooRealVar(ratioName,ratioName,(1.0/nMCsamples),0.0,1.0) )
	ArgList_Ratios.add( ratios[-1] )

doRecursive = True 
combinedSampleFractions = ROOT.RooAddPdf( "combinedSampleFractions" , "combinedSampleFractions" , ArgList_ClusterDistributions , ArgList_Ratios ) #, doRecursive ) 

# Add the pseudo-data 

histName = "fractions_PseudoData"
pseudoDataHist = ROOT.TH1D(histName,histName,nClusters,-0.5,nClusters-0.5) 	
pseudoDataHist.Sumw2() 
for iCluster in range(nClusters):
	pseudoDataHist.SetBinContent( pseudoDataHist.FindBin( iCluster ) , fullPseudoDataFractionPerCluster[iCluster] )
	pseudoDataHist.SetBinError( pseudoDataHist.FindBin( iCluster ) , errPseudoDataFractionsPerCluster[iCluster] )
pseudoDataRooHist = ROOT.RooDataHist(histName, histName, argList, pseudoDataHist) 

# Do the fit 

result = combinedSampleFractions.fitTo( pseudoDataRooHist, ROOT.RooFit.SumW2Error(1) ) 

### Get the True Composition ###

trueResults = [] 
totalNevents = sum(nPDevents)
totalWeight = sum( allPseudoDataWeights )
for array in pseudoDataWeights:
	#trueResults.append( len(array) / totalNevents )
	trueResults.append( np.sum(array) / totalWeight )
	
### Print the Results ###

print( "----- FOUND COMPOSITION ESTIMATE ----- " )
print( "MC Samples Accepted: " )
print( "\t", labels )
for iSample in range(len(pseudoData)): 
	print( "\t# PseudoData events from MC Sample " , iSample , " = " , len(pseudoData[iSample]) )
for iSample in range(len(monteCarlo)): 
	print( "\t# Monte Carlo events from MC Sample " , iSample , " = " , len(monteCarlo[iSample]) )
print( "Prediction: " )
for iSample in range(nMCsamples):
	ArgList_Ratios[iSample].Print()  
print( "Truth: " ) 
print( "\t" , trueResults ) 


### Make a Plot of the Templates ### 

xframe = clusterIDs.frame() 
myColors = range( 800, 910, int( np.floor( 110/nClusters ) ) ) 
#myColors = [ 2 , 4 ] 
canvas = ROOT.TCanvas("canvas","canvas",600,600) 
myLegend = ROOT.TLegend(0.65,0.65,0.89,0.89) 

for iSample in range(nMCsamples): 
	fractionRooHists[ iSample ].plotOn( xframe , ROOT.RooFit.LineColor( myColors[ iSample ] ) , 
												 ROOT.RooFit.MarkerColor( myColors[ iSample ] ) , 
												 ROOT.RooFit.Name( "rooHist_"+str(iSample) )  ) 
	myLegend.AddEntry( xframe.findObject( "rooHist_"+str(iSample) ) , labels[ iSample ] )
pseudoDataRooHist.plotOn( xframe ) 
myLegend.AddEntry( pseudoDataRooHist , "Data" )
xframe.Draw() 
myLegend.Draw() 

canvas.Update()


