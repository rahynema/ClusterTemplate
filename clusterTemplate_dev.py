### Import Necessary Packages ###

import ROOT 
import numpy as np
from root_numpy import root2array, tree2array, rec2array
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
from sklearn.metrics import calinski_harabaz_score, silhouette_score
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors
import sys
sys.path.append('/Users/rhyneman/Work/PythonPlottingScripts')
import DrawNiceHistograms


### Here Begin the Sub-Functions Used ###

def readInputFiles( inFileNames , selection2apply , branches2read , jet_branches2read ): 

	inputDir = 'h019inputs/'

	# Get the input MC files 
	inFiles = []
	for fileName in inFileNames: inFiles.append( ROOT.TFile.Open( inputDir+fileName+'.root' ) )

	# Get the Trees 
	inTreeName = 'ttHyyTree'

	inTrees = []
	for file in inFiles: 
		inTrees.append( file.Get( inTreeName ) )

	branches2read = [ 'weight_total' , 'bdt_category_weight' ] + branches2read 
	varItr = 3 
	allBranches = branches2read + jet_branches2read
	nClustersMax = 20 

	# Construct the Monte Carlo Array

	strucArrays = []
	jetInfoArrays = [] 
	arrays = [] 
	weights = [] 
	for i,tree in enumerate(inTrees): 
		strucArrays.append( tree2array( tree ,
								branches = branches2read,
								selection = selection2apply ) )
		jetInfoArrays.append( tree2array( tree ,
								branches = jet_branches2read,
								selection = selection2apply ) )
		holdJetInfoArray = np.ones( (len(jetInfoArrays[-1]) , len(jet_branches2read) ) )
		holdJetInfoArray = np.multiply( holdJetInfoArray , -1.0 )
		for row in range(len(jetInfoArrays[-1])):
			for col in range(len(jet_branches2read)):
				if ( jetInfoArrays[-1][row][col] ): holdJetInfoArray[row][col] = jetInfoArrays[-1][row][col]
		arrays.append( np.column_stack( [ rec2array( strucArrays[-1] ) , np.array(holdJetInfoArray) ] ) )
		if( len(arrays[-1]) < nClustersMax ): print( "WARNING: Few events in input sample ", inFileNames[i] )  # del arrays[-1]
		weights.append( np.multiply( arrays[-1][:,0] , arrays[-1][:,1] ) ) 
			
		#for file in inFiles: file.Close() 

	return arrays, weights  



def getDataPCA( scaledData ): 

	dataPCA = PCA(n_components=2)
	dataPCA.fit( scaledData )
	reducedData = dataPCA.transform( scaledData )

	return reducedData , dataPCA



def applyDataPCA( scaledArrays , dataPCA ): 

	reducedArrays = []
	for array in scaledArrays: 
		reducedArrays.append( dataPCA.transform( array ) )
		
	return reducedArrays 



def getBestNClusters( scaled_DataArray ): 
	
	testVariances = [] 
	testScores = [] 
	testScores2 = [] 
	
	for nTest in range(2,45): 
		clusterNTest = AgglomerativeClustering( n_clusters=nTest )
		clusterNTest.fit( scaled_DataArray )
		holdVariances = []
		for icls in range(nTest):
			holdVariances.append( scaled_DataArray[ clusterNTest.labels_==icls ] )
		testVariances.append( [ nTest , np.sum( np.sqrt( holdVariances[0]*holdVariances[0] ) )/nTest ] )
		#testScores.append( silhouette_score( scaled_DataArray , clusterNTest.labels_ ) )
		testScores2.append( calinski_harabaz_score( scaled_DataArray , clusterNTest.labels_ ) ) 
	testVariances = np.array( testVariances )
	
#	nClusterFit = np.polyfit( testVariances[:,0], np.log(testVariances[:,1]), 1, w=np.sqrt(testVariances[:,1]) )
# 	curveFit = [ np.e**(nClusterFit[0]*x) * np.e**(nClusterFit[1]) for x in testVariances[:,0] ] 
# 	curveFit_1Deriv = [ np.e**(nClusterFit[0]*x) * np.e**(nClusterFit[1]) * (nClusterFit[0]) for x in testVariances[:,0] ] 
# 	curveFit_2Deriv = [ np.e**(nClusterFit[0]*x) * np.e**(nClusterFit[1]) * (nClusterFit[0])*(nClusterFit[0]) for x in testVariances[:,0] ] 
#	curveFit = np.array( curveFit )
		
	plt.figure()
	#plt.plot( testVariances[:,0] , testScores2[:] , c='red' ) 
	plt.plot( testVariances[:,0] ,testVariances[:,1] , c='red' ) 
	plt.show() 
	
	nBestClusters = 2 #HOLDER 
	
	return nBestClusters 



def applyDataClustering( scaled_DataArray , nClusters , nNeighbors ): 
	
	clusterData = AgglomerativeClustering( n_clusters=nClusters , linkage='ward' )
	clusterData.fit( scaled_DataArray ) 

	nearestNeighbors = KNeighborsClassifier(weights='distance',n_neighbors=nNeighbors)
	nearestNeighbors.fit( scaled_DataArray , clusterData.labels_ )

	return clusterData , nearestNeighbors 
	


def applyNNeighbors( nearestNeighbors , scaledArrays ): 

	clusteredArrays = []
	for array in scaledArrays: 
		clusteredArrays.append( nearestNeighbors.predict( array ) )
	
	return clusteredArrays 



def getMCErrors( weights , testLabels , nClusters ): 

	nSubSampels = len( weights )
	nTests = len( testLabels )
	
	avgFractionsPerCluster = []
	errFractionsPerCluster = []

	for iSample in range(nSubSampels): 
		holdFractions = [] 
		totalWeight = np.sum( weights[iSample] )
		for iTest in range( nTests ): 
			testFractions = []
			for cluster in range(nClusters): 			
				testFractions.append( np.sum( weights[iSample][testLabels[iTest][iSample]==cluster] ) / totalWeight )
			holdFractions.append( testFractions )
		holdFractions = np.array( holdFractions )
		
		avgFractionsPerCluster.append( np.mean(holdFractions,0) )
		errFractionsPerCluster.append( np.std(holdFractions,0) )

	if( nSubSampels == 1 ): return errFractionsPerCluster[0] 
	else: return errFractionsPerCluster



def getPDErrors( weights , testLabels , nClusters ): 

	nTests = len( testLabels )
	
	avgFractionsPerCluster = []
	errFractionsPerCluster = []

	holdFractions = [] 
	totalWeight = np.sum( weights )
	for iTest in range( nTests ): 
		testFractions = []
		for cluster in range(nClusters): 
			testFractions.append( np.sum( weights[testLabels[iTest][:]==cluster] ) / totalWeight )
		holdFractions.append( testFractions )
	holdFractions = np.array( holdFractions )
	
	avgFractionsPerCluster.append( np.mean(holdFractions,0) )
	errFractionsPerCluster.append( np.std(holdFractions,0) )

	return errFractionsPerCluster[0] 



def getCompositionByCluster( weights , labels , nClusters ): 

	nSubSampels = len( weights )
	fractionPerCluster = []

	for iSample in range(nSubSampels): 
		holdEventsPerBin = []
		totalWeight = np.sum( weights[iSample] )
		for cluster in range(nClusters): 
			holdEventsPerBin.append( np.sum( weights[iSample][labels[iSample][:]==cluster] ) / totalWeight )
		fractionPerCluster.append( holdEventsPerBin ) 	
		
	if( nSubSampels == 1 ): return fractionPerCluster[0] 
	else: return fractionPerCluster



def getClusterStatErrors( cluster_labels, nClusters ): 

	errorPerCluster = []

	for cluster in range(nClusters): 
		if( len( cluster_labels[cluster_labels[:]==cluster] ) == 0 ): errorPerCluster.append( 0.0 )
		else: errorPerCluster.append( np.sqrt( 1.0 / len( cluster_labels[cluster_labels[:]==cluster] ) ) )
	
	return errorPerCluster



### Below is for Optimization with Roofit ### 

def doOptimization( nClusters , mcFractionPerCluster , mcClusterStatErrors , dataFractionPerCluster , dataClusterStatErrors, labels ):
	clusterBins = [ x for x in range(nClusters) ]
	clusterIDs = np.array( clusterBins )
	clusterIDs = ROOT.RooRealVar("clusterIDs","clusterIDs",0,nClusters-1)
	argList = ROOT.RooArgList(clusterIDs)
	nMCsamples = len(labels) 

	fractionHists = []
	fractionRooHists = [] 
	fractionRooHistPdfs = []
	
	ArgList_ClusterDistributions = ROOT.RooArgList()
	for iSample in range(len(mcFractionPerCluster)): 
		histName = "fractions_" + labels[iSample]
		fractionHists.append( ROOT.TH1D(histName,histName,nClusters,-0.5,nClusters-0.5) )
		fractionHists[-1].SetEntries(nClusters)
		fractionHists[-1].Sumw2()
		for iCluster in range(nClusters):
			fractionHists[-1].AddBinContent( fractionHists[-1].FindBin( iCluster ) , mcFractionPerCluster[iSample][iCluster] )
			theError = np.sqrt( mcClusterStatErrors[iSample][iCluster]*mcClusterStatErrors[iSample][iCluster] ) 
			fractionHists[-1].SetBinError( fractionHists[-1].FindBin( iCluster ) , theError  )	
		fractionRooHists.append( ROOT.RooDataHist(histName, histName, argList, fractionHists[-1]) )
		histName = "fractionPdfs_" + labels[iSample]
		fractionRooHistPdfs.append( ROOT.RooHistPdf( histName, histName, ROOT.RooArgSet(clusterIDs), fractionRooHists[-1] )  )
		ArgList_ClusterDistributions.add( fractionRooHistPdfs[-1] )

	ArgList_Ratios = ROOT.RooArgList()
	ratios = []
	for iSample in range(len(mcFractionPerCluster)): 	
		ratioName = "Composition_"+labels[iSample]
		ratios.append( ROOT.RooRealVar(ratioName,ratioName,(1.0/nMCsamples),0.0,1.0) )
		ArgList_Ratios.add( ratios[-1] )

	combinedSampleFractions = ROOT.RooAddPdf( "combinedSampleFractions" , "combinedSampleFractions" , ArgList_ClusterDistributions , ArgList_Ratios ) 

	# Add the data 

	histName = "fractions_data"
	dataHist = ROOT.TH1D(histName,histName,nClusters,-0.5,nClusters-0.5) 
	dataHist.SetEntries(nClusters)	
	dataHist.Sumw2() 
	for iCluster in range(nClusters):
		dataHist.SetBinContent( dataHist.FindBin( iCluster ) , dataFractionPerCluster[iCluster] )
		dataHist.SetBinError( dataHist.FindBin( iCluster ) , dataClusterStatErrors[iCluster] )
	dataRooHist = ROOT.RooDataHist(histName, histName, argList, dataHist) 

	# Do the fit 

	result = combinedSampleFractions.fitTo( dataRooHist, ROOT.RooFit.SumW2Error(1) ) 
	
	return( dataRooHist , ratios , fractionRooHists , fractionHists )


### Below are Plotting Functions ### 

def getNiceAxes( arrays ): 

	xMax = -9999 
	xMin = 9999
	yMax = -9999 
	yMin = 9999 

	for array in arrays: 
		if( xMin > np.min( array[:,0] ) ): xMin = np.min( array[:,0] )
		if( xMax < np.max( array[:,0] ) ): xMax = np.max( array[:,0] )
		if( yMin > np.min( array[:,1] ) ): yMin = np.min( array[:,1] )
		if( yMax < np.max( array[:,1] ) ): yMax = np.max( array[:,1] )

	xMin = 1.2*xMin
	xMax = 1.2*xMax
	yMin = 1.2*yMin
	yMax = 1.2*yMax

	return xMin,xMax,yMin,yMax 



def makeClusterPCAplot_MC( reduced_mcArrays , clusterLabels , legendLabels , whichColors , plotRange , plotName ): 
	
	xMin = plotRange[0] 
	xMax = plotRange[1] 
	yMin = plotRange[2] 
	yMax = plotRange[3]
	
	markerList = [ 'o' , 'd' , 'D' , '+' , 's' , 'v' , '^' , '<' , 'D' , 'd' , 'o' , 'o' ,'o' ]
	
	plt.figure(figsize=[7,7])
	for i,array in enumerate(reduced_mcArrays): 
		thePlotColors = [ whichColors[i] for i in clusterLabels[i] ]
		plt.scatter(   array[:,0] , 
					   array[:,1] , 
					   c = thePlotColors ,
					   marker = markerList[i] )

	plt.title('Clustering of MC Events in PCA Space')
	plt.ylabel('PCA Axis #1')
	plt.xlabel('PCA Axis #2')
	plt.xlim( [xMin, xMax] )
	plt.ylim( [yMin, yMax] )
	plt.legend(legendLabels)
	plt.xticks(())
	plt.yticks(())
	if( plotName != "" ): plt.savefig(plotName+".png")
	plt.show()



def makeClusterPCAplot_Data( reducedArray , clusterLabels , whichColors , plotRange , plotName ): 
	
	xMin = plotRange[0] 
	xMax = plotRange[1] 
	yMin = plotRange[2] 
	yMax = plotRange[3]
	
	thePlotColors = [ whichColors[i] for i in clusterLabels ]
	
	plt.figure(figsize=[7,7])
	plt.scatter(   reducedArray[:,0] , 
				   reducedArray[:,1] , 
				   c = thePlotColors ,
				   marker = "*" )
	
	plt.title('Clustering of Data in PCA Space')
	plt.ylabel('PCA Axis #1')
	plt.xlabel('PCA Axis #2')
	plt.xlim( [xMin, xMax] )
	plt.ylim( [yMin, yMax] )
	plt.xticks(())
	plt.yticks(())
	if( plotName != "" ): plt.savefig(plotName+".png")
	plt.show()



### Make a Plot of the Templates ### 
def plotClusterTemplates( dataRooHist , fractionHists , labels ): 
	
	nMCsamples = len( fractionHists ) 
	
	orderedClusterIDs = []  

	holderHisto = dataRooHist.createHistogram("clusterIDs")
	nClusters = holderHisto.GetNbinsX() 
	for i in range(nClusters): 
		orderedClusterIDs.append([ holderHisto.GetBinContent(i+1) , i+1 ])
	orderedClusterIDs = np.array(orderedClusterIDs)
	orderedClusterIDs = orderedClusterIDs[orderedClusterIDs[:,0].argsort()]
	orderedClusterIDs= orderedClusterIDs[::-1]

	# Order the pseudo-data hist 

	pseudoDataHisto = ROOT.TH1D("pseudoDataHisto","pseudoDataHisto",nClusters,-0.5,nClusters-0.5) 
	pseudoDataHisto.Sumw2() 
	pseudoDataHisto.SetEntries(nClusters)	
	for iEntry in range(len(orderedClusterIDs)):
		pseudoDataHisto.SetBinContent( pseudoDataHisto.FindBin( iEntry ) , holderHisto.GetBinContent(int(orderedClusterIDs[iEntry][1])) )
		pseudoDataHisto.SetBinError( pseudoDataHisto.FindBin( iEntry ) , holderHisto.GetBinError(int(orderedClusterIDs[iEntry][1])) )

	orderedMChistos = [] 
	for iSample in range(nMCsamples): 
		hName = "orderedHisto_" + labels[iSample] 
		orderedMChistos.append( ROOT.TH1D(hName,hName,nClusters,-0.5,nClusters-0.5) )
		holderHisto = fractionHists[iSample]
		for iEntry in range(len(orderedClusterIDs)):
			orderedMChistos[-1].SetBinContent( orderedMChistos[-1].FindBin( iEntry ) , holderHisto.GetBinContent(int(orderedClusterIDs[iEntry][1])) )
			orderedMChistos[-1].SetBinError( orderedMChistos[-1].FindBin( iEntry ) , holderHisto.GetBinError(int(orderedClusterIDs[iEntry][1])) )


	canvas = ROOT.TCanvas("canvas","canvas",600,600)
	canvas, hists_to_plot, legend = DrawNiceHistograms.DrawNiceHistograms( 
			histograms = orderedMChistos, 
			legend_labels = labels,
			ATLASstyle = True,
			drawopt = "ehist", 
			linewidth = 2, 
			markersize = 0, 
			xtitle = "Cluster ID", 
			ytitle = "Fraction", 
			title = "Cluster Templates",
			xrange = [-0.5,nClusters-0.5] ,
			yrange = [0,0.45] )

	canvas.cd() 
	pseudoDataHisto.SetMarkerColor(1) 
	pseudoDataHisto.SetMarkerStyle(8)
	pseudoDataHisto.SetLineColor(1) 
	pseudoDataHisto.SetLineWidth(2) 
	pseudoDataHisto.Draw("ep same")
	legend.AddEntry( pseudoDataHisto , "Pseudo-Data" ) 
	legend.Draw()
	canvas.Update()
	canvas.Print("ClusterTemplates.png")


### Below is for Performing the Full Template Method ### 

def predictClusterTemplateComposition( inputData , inputMC , mcWeights , nClusters , nNeighbors , labels , allBranches , makePlots ): 
	
	nMCsamples = len( inputMC ) 
	varItr = 3 
	
	# Normalize the data inputs 
	scaledData = [] 
	scaledData = normalize( np.vstack( inputData )[:,varItr:] ) 
	
	# Normalize the MC inputs 
	scaledMC = []
	for array in inputMC: 
		scaledMC.append( normalize( array[:,varItr:] ) )
		
	# Get the PCA Axes from the data 
	reducedData , dataPCA = getDataPCA( scaledData )  
	# Apply the PCA from Data onto MC
	reducedMC = applyDataPCA( scaledMC , dataPCA )
	
	# Print Info on the PCA Components 
	print( "PCA Components: " )
	print( "   --- Axis #1 --- " )
	indeces = np.argpartition( dataPCA.components_[0], -5 )[-5:]
	for i in range(5): 
		print( "   " , allBranches[indeces[i]] , " - " , dataPCA.components_[0][indeces[i]] )
	print( "   --- Axis #2 --- " )
	indeces = np.argpartition( dataPCA.components_[1], -5 )[-5:]
	for i in range(5): 
		print( "   " , allBranches[indeces[i]] , " - " , dataPCA.components_[1][indeces[i]] )
	
	# Make the Cluster Template
	dataClustering, nearestNeighbors = applyDataClustering( scaledData , nClusters , nNeighbors ) 
	mcLabels = applyNNeighbors( nearestNeighbors , scaledMC )
	
	# Get the Statistical Errors for the Clusters 
	mcStatErrors = []
	for iSample in range( nMCsamples ):
		mcStatErrors.append( getClusterStatErrors( mcLabels[iSample], nClusters ) )
	dataStatErrors = getClusterStatErrors( dataClustering.labels_, nClusters )
	
	# Get the Distribution of Samples in Clusters (the Templates) 
	fullMCFractionPerCluster = getCompositionByCluster( mcWeights , mcLabels , nClusters ) 
	fullDataFractionPerCluster = getCompositionByCluster( [ np.ones(len(dataClustering.labels_)) ] , [ dataClustering.labels_ ] , nClusters )  

	# Do the Optimization 
	[ dataRooHist , ArgList_Ratios , fractionRooHists , fractionHists ] = doOptimization( 
		nClusters , 
		fullMCFractionPerCluster , 
		mcStatErrors , 
		fullDataFractionPerCluster , 
		dataStatErrors, 
		labels ) 
		
			
	if( makePlots ): 
		cm = plt.get_cmap('gist_rainbow')
		cNorm  = colors.Normalize(vmin=0, vmax=nClusters-1)
		scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)
		myColors = [ scalarMap.to_rgba(i) for i in range(nClusters) ]
		
		sharedAxes = getNiceAxes( reducedMC + [ reducedData ] )

		makeClusterPCAplot_Data( reducedData , dataClustering.labels_ , myColors , sharedAxes , "dataClustersPCA" )
		makeClusterPCAplot_MC( reducedMC , mcLabels , labels , myColors , sharedAxes , "mcClustersPCA" ) 

		
	return( dataClustering , dataRooHist , ArgList_Ratios , fractionRooHists , fractionHists ) 


### Below is for Performing the Full Template Method ### 

def predictPseudoDataClusterTemplateComposition( inputData , inputPseudoData , pseudoDataWeights , inputMC , mcWeights , nClusters , nNeighbors , labels , allBranches , makePlots ): 
	
	nMCsamples = len( inputMC ) 
	varItr = 3 
	
	# Normalize the data inputs 
	scaledData = [] 
	scaledData = normalize( np.vstack( inputData )[:,varItr:] ) 

	# Normalize the pseudodata inputs 
	scaledPseudoData = [] 
	for array in inputPseudoData: 
		scaledPseudoData.append( normalize( array[:,varItr:] ) )
	
	# Normalize the MC inputs 
	scaledMC = []
	for array in inputMC: 
		scaledMC.append( normalize( array[:,varItr:] ) )
		
	# Get the PCA Axes from the data 
	reducedData , dataPCA = getDataPCA( scaledData )  
	# Apply the PCA from Data onto PseudoData
	reducedPseudoData = applyDataPCA( scaledPseudoData , dataPCA )
	# Apply the PCA from Data onto MC
	reducedMC = applyDataPCA( scaledMC , dataPCA )
	
	# Print Info on the PCA Components 
	print( "PCA Components: " )
	print( "   --- Axis #1 --- " )
	indeces = np.argpartition( dataPCA.components_[0], -5 )[-5:]
	for i in range(5): 
		print( "   " , allBranches[indeces[i]] , " - " , dataPCA.components_[0][indeces[i]] )
	print( "   --- Axis #2 --- " )
	indeces = np.argpartition( dataPCA.components_[1], -5 )[-5:]
	for i in range(5): 
		print( "   " , allBranches[indeces[i]] , " - " , dataPCA.components_[1][indeces[i]] )
	
	# Make the Cluster Template
	dataClustering, nearestNeighbors = applyDataClustering( scaledData , nClusters , nNeighbors ) 
	pseudoDataLabels = applyNNeighbors( nearestNeighbors , scaledPseudoData )
	mcLabels = applyNNeighbors( nearestNeighbors , scaledMC )
	
	# Get the Statistical Errors for the Clusters 
	mcStatErrors = []
	for iSample in range( nMCsamples ):
		mcStatErrors.append( getClusterStatErrors( mcLabels[iSample], nClusters ) )
	pseudoDataStatErrors = getClusterStatErrors( np.hstack(pseudoDataLabels), nClusters ) 
	dataStatErrors = getClusterStatErrors( dataClustering.labels_, nClusters )
	
	# Get the Distribution of Samples in Clusters (the Templates) 
	fullMCFractionPerCluster = getCompositionByCluster( mcWeights , mcLabels , nClusters ) 
	fullPseudoDataFractionPerCluster = getCompositionByCluster( [ np.hstack(pseudoDataWeights) ] , [ np.hstack(pseudoDataLabels) ] , nClusters ) 
	fullDataFractionPerCluster = getCompositionByCluster( [ np.ones(len(dataClustering.labels_)) ] , [ dataClustering.labels_ ] , nClusters )  

	# Do the Optimization 
	[ pseudoDataRooHist , ArgList_Ratios , fractionRooHists , fractionHists ] = doOptimization( 
		nClusters , 
		fullMCFractionPerCluster , 
		mcStatErrors , 
		fullPseudoDataFractionPerCluster , 
		pseudoDataStatErrors, 
		labels ) 
		
			
	if( makePlots ): 
		cm = plt.get_cmap('gist_rainbow')
		cNorm  = colors.Normalize(vmin=0, vmax=nClusters-1)
		scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)
		myColors = [ scalarMap.to_rgba(i) for i in range(nClusters) ]
		
		sharedAxes = getNiceAxes( reducedMC + [ reducedData ] )

		makeClusterPCAplot_Data( reducedData , dataClustering.labels_ , myColors , sharedAxes , "dataClustersPCA" )
		makeClusterPCAplot_MC( reducedPseudoData , pseudoDataLabels , labels , myColors , sharedAxes , "PseudoDataClustersPCA" )
		makeClusterPCAplot_MC( reducedMC , mcLabels , labels , myColors , sharedAxes , "mcClustersPCA" ) 

	return( dataClustering , pseudoDataRooHist , ArgList_Ratios , fractionRooHists , fractionHists ) 


### Below is to get N_Clusters and N_Neighbors systematics ###


def getClusterTemplateSystematics( nominalFractions , inputData , inputMC , mcWeights , nClusters , nNeighbors ): 
	
	nMCsamples = len( inputMC ) 
	allSyst_combinedSampleFractions = [] 
	
	allSyst_combinedSampleFractions.append( predictClusterTemplateComposition( inputData , inputMC , mcWeights , nClusters , nNeighbors , labels )[1] )
	nClusters_systUp = nClusters + 1 
	allSyst_combinedSampleFractions.append( predictClusterTemplateComposition( inputData , inputMC , mcWeights , nClusters_systUp , nNeighbors , labels )[1] )
	nClusters_systDown = nClusters - 1 
	allSyst_combinedSampleFractions.append( predictClusterTemplateComposition( inputData , inputMC , mcWeights , nClusters_systDown , nNeighbors , labels )[1] )
	nNeighbors_systUp = nNeighbors + 5
	allSyst_combinedSampleFractions.append( predictClusterTemplateComposition( inputData , inputMC , mcWeights , nClusters , nNeighbors_systUp , labels )[1] )
	nNeighbors_systDown = nNeighbors - 5
	allSyst_combinedSampleFractions.append( predictClusterTemplateComposition( inputData , inputMC , mcWeights , nClusters , nNeighbors_systDown , labels )[1] )
	
	# Four variations in Two Categories 
	
	nClusterSyst = 0.0 
	nNeighborsSyst = 0.0 
 	
# 	for iSample in nMCsamples: 
# 		ArgList_Ratios[iSample].getValV()
	
	return allSyst_combinedSampleFractions	
	

### Here are the Main Functions ### 

def clusterPseudoData(): 

	# Define the input files used 
	in_Data_FileNames = [ "data/data15" , "data/data16" , "data/data17" ] 
	in_MC_FileNames = [ "mc16a/Sherpa_2DP20_myy_all" , # Sherpa yy  
						"mc16d/Sherpa_2DP20_myy_all" , # Sherpa yy 
						"mc16a/Sherpa_SinglePhoton_all" , # Sherpa yj 
						"mc16d/Sherpa_SinglePhoton_all" , # Sherpa yj 
						"mc16a/MGPy8_ttgamma_nonallhadronic" , # tt+y 
						"mc16d/MGPy8_ttgamma_nonallhadronic" , # tt+y 
						"mc16a/MGPy8_ttgammagamma_noallhad" , # tt+yy 
						"mc16d/MGPy8_ttgammagamma_noallhad" ] # tt+yy 

	# Define which input variables will be used 
	branches2read = [ 'mT' , 'n_jets25' , 'n_jets25_central' , 'n_bjets25_77' ]
	jet_branches2read = [ 	'jet_btag77[0]'  , 'jet_btag77[1]' ]
	allBranches = branches2read + jet_branches2read

	# Define the event selection
	selection2apply = '(m_yy<160&&m_yy>105&&(n_jets25>=3)&&(n_bjets25_77>=1)&&n_lep==0)' #&&y1_is_isolated&&y2_is_isolated&&y1_is_tight&&y2_is_tight)'    
	selection2apply_TI = '(m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&(n_jets25>=3)&&(n_bjets25_77>=1)&&y1_is_isolated&&y2_is_isolated&&y1_is_tight&&y2_is_tight&&n_lep==0)'    
	selection2apply_NTI = '(m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&(n_jets25>=3)&&(n_bjets25_77>=1)&&!y1_is_isolated&&!y2_is_isolated&&!y1_is_tight&&!y2_is_tight&&n_lep==0)'    

	# Generate the data arrays 
	data_Arrays = readInputFiles( in_Data_FileNames , selection2apply_TI , branches2read , jet_branches2read )[0:1][0]
	data = np.vstack(data_Arrays)
	mc_Arrays, weights_mcArrays  = readInputFiles( in_MC_FileNames , selection2apply , branches2read , jet_branches2read )

	# Generate the MC arrays 
	labels = in_MC_FileNames
	nMCsamples = len(labels) 

	# Split the Monte Carlo into "pseudo-data" and reference MC events 
	nPDevents = [ 100000 , 100000 , 100000 , 100000 , 100000 , 100000 , 100000 , 100000 ] 
	pseudoData = []
	monteCarlo = []
	mcWeights = [] 
	pseudoDataWeights = []
	for iSample in range( nMCsamples ): 
		if( len( mc_Arrays[iSample] ) < nPDevents[iSample] ): 
			print( "WARNING: Not enough events in MC Sample: ", labels[iSample], " - will default to half sample size in pseudo-data. " )
			nPDevents[iSample] = int( np.floor( len( mc_Arrays[iSample] ) / 2.0 ) )
		pseudoData.append( mc_Arrays[iSample][:nPDevents[iSample]] )
		monteCarlo.append( mc_Arrays[iSample][nPDevents[iSample]:] )
		pseudoDataWeights.append( weights_mcArrays[iSample][:nPDevents[iSample]] )
		mcWeights.append( weights_mcArrays[iSample][nPDevents[iSample]:] )
	
	# Merge the MC samples into larger subsamples 
	monteCarlo = [ np.vstack((monteCarlo[0],monteCarlo[1],monteCarlo[2],monteCarlo[3])) ] + [ np.vstack((monteCarlo[4],monteCarlo[5],monteCarlo[6],monteCarlo[7])) ]
	mcWeights[0] = np.multiply( mcWeights[0] , 36.1 ) 
	mcWeights[1] = np.multiply( mcWeights[1] , 43.0 ) 
	mcWeights[2] = np.multiply( mcWeights[2] , 36.1 ) 
	mcWeights[3] = np.multiply( mcWeights[3] , 43.0 ) 
	mcWeights[4] = np.multiply( mcWeights[4] , 36.1 ) 
	mcWeights[5] = np.multiply( mcWeights[5] , 43.0 ) 
	mcWeights[6] = np.multiply( mcWeights[6] , 36.1 ) 
	mcWeights[7] = np.multiply( mcWeights[7] , 43.0 ) 
	mcWeights = [ np.hstack((mcWeights[0],mcWeights[1],mcWeights[2],mcWeights[3])) ] + [ np.hstack((mcWeights[4],mcWeights[5],mcWeights[6],mcWeights[7])) ]
	labels = ["Photon QCD" , "tt+y(y)" ] 
	nMCsamples = len(labels) 
	
	# Merge the PseudoData samples into larger subsamples 
	pseudoData = [ np.vstack((pseudoData[0],pseudoData[1],pseudoData[2],pseudoData[3])) ] + [ np.vstack((pseudoData[4],pseudoData[5],pseudoData[6],pseudoData[7])) ]
	pseudoDataWeights[0] = np.multiply( pseudoDataWeights[0] , 36.1 ) 
	pseudoDataWeights[1] = np.multiply( pseudoDataWeights[1] , 43.0 ) 
	pseudoDataWeights[2] = np.multiply( pseudoDataWeights[2] , 36.1 ) 
	pseudoDataWeights[3] = np.multiply( pseudoDataWeights[3] , 43.0 ) 
	pseudoDataWeights[4] = np.multiply( pseudoDataWeights[4] , 36.1 ) 
	pseudoDataWeights[5] = np.multiply( pseudoDataWeights[5] , 43.0 ) 
	pseudoDataWeights[6] = np.multiply( pseudoDataWeights[6] , 36.1 ) 
	pseudoDataWeights[7] = np.multiply( pseudoDataWeights[7] , 43.0 ) 
	pseudoDataWeights = [ np.hstack((pseudoDataWeights[0],pseudoDataWeights[1],pseudoDataWeights[2],pseudoDataWeights[3])) ] + [ np.hstack((pseudoDataWeights[4],pseudoDataWeights[5],pseudoDataWeights[6],pseudoDataWeights[7])) ]
	
	nClusters = 6
	nNeighbors = 25

	### Perform the Optimization ###
	
	makePlots = True   
	pseudoDataClustering , pseudoDataRooHist , ArgList_Ratios , fractionRooHists , fractionHists = predictPseudoDataClusterTemplateComposition( data , pseudoData , pseudoDataWeights , monteCarlo , mcWeights , nClusters , nNeighbors , labels , allBranches , makePlots )
	
	### Plot the Cluster Templates ### 
	
	plotClusterTemplates( pseudoDataRooHist , fractionHists , labels ) 
	
	### Get the True Composition ###

	trueResults = [] 
	totalNevents = sum(nPDevents)
	totalWeight = sum( np.hstack( pseudoDataWeights )  )
	for array in pseudoDataWeights:
		trueResults.append( np.sum(array) / totalWeight )
	
	### Print the Results ###

	print( "----- FOUND COMPOSITION ESTIMATE ----- " )
	print( "MC Samples Accepted: " )
	print( "\t", labels )
	for iSample in range(len(pseudoData)): 
		print( "\t# PseudoData events from MC Sample " , iSample , " = " , len(pseudoData[iSample]) )
	for iSample in range(len(monteCarlo)): 
		print( "\t# Monte Carlo events from MC Sample " , iSample , " = " , len(monteCarlo[iSample]) )
	print( "Prediction: " )
	for iSample in range(nMCsamples):
		print( ArgList_Ratios[iSample].getValV() , " +/- " , ArgList_Ratios[iSample].getError() )  #ratios_fullSystErrors[iSample] ) 
	print( "Truth: " ) 
	print( "\t" , trueResults ) 
	print( "   " ) 
	print( "   " ) 


def clusterData(): 

	# Define the input files used 
	in_Data_FileNames = [ "data/data15" , "data/data16" ]#, "data/data17" ] 
	in_MC_FileNames = [ "mc16a/Sherpa_2DP20_myy_all" , # Sherpa yy  
						"mc16d/Sherpa_2DP20_myy_all" , # Sherpa yy 
						"mc16a/Sherpa_SinglePhoton_all" , # Sherpa yj 
						"mc16d/Sherpa_SinglePhoton_all" , # Sherpa yj 
						"mc16a/MGPy8_ttgamma_nonallhadronic" , # tt+y 
						"mc16d/MGPy8_ttgamma_nonallhadronic" , # tt+y 
						"mc16a/MGPy8_ttgammagamma_noallhad" , # tt+yy 
						"mc16d/MGPy8_ttgammagamma_noallhad" ] # tt+yy 

	# Define which input variables will be used 
	branches2read = [ 'mT' , 'n_jets25' , 'n_jets25_central' , 'n_bjets25_77' , 'pTlepMET' ]
	jet_branches2read = [ 	'jet_btag77[0]'  , 'jet_btag77[1]' ]
	allBranches = branches2read + jet_branches2read

	# Define the event selection
	selection2apply = '(m_yy<160&&m_yy>105&&(n_jets25>=3)&&(n_bjets25_77>=1)&&n_lep==0)' #&&y1_is_isolated&&y2_is_isolated&&y1_is_tight&&y2_is_tight)'    
	selection2apply_TI = '(m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&(n_jets25>=3)&&(n_bjets25_77>=1)&&y1_is_isolated&&y2_is_isolated&&y1_is_tight&&y2_is_tight&&n_lep==0)'    
	selection2apply_NTI = '(m_yy<160&&m_yy>105&&(m_yy<120||m_yy>130)&&(n_jets25>=3)&&(n_bjets25_77>=1)&&!y1_is_isolated&&!y2_is_isolated&&!y1_is_tight&&!y2_is_tight&&n_lep==0)'    

	# Generate the data and MC arrays 
	data = readInputFiles( in_Data_FileNames , selection2apply_TI , branches2read , jet_branches2read )[0:1][0]
	monteCarlo, mcWeights  = readInputFiles( in_MC_FileNames , selection2apply , branches2read , jet_branches2read )
	
	# Merge the MC samples into larger subsamples 
	monteCarlo = [ np.vstack((monteCarlo[0],monteCarlo[1],monteCarlo[2],monteCarlo[3])) ] + [ np.vstack((monteCarlo[4],monteCarlo[5],monteCarlo[6],monteCarlo[7])) ]
	mcWeights[0] = np.multiply( mcWeights[0] , 36.1 ) 
	mcWeights[1] = np.multiply( mcWeights[1] , 43.0 ) 
	mcWeights[2] = np.multiply( mcWeights[2] , 36.1 ) 
	mcWeights[3] = np.multiply( mcWeights[3] , 43.0 ) 
	mcWeights[4] = np.multiply( mcWeights[4] , 36.1 ) 
	mcWeights[5] = np.multiply( mcWeights[5] , 43.0 ) 
	mcWeights[6] = np.multiply( mcWeights[6] , 36.1 ) 
	mcWeights[7] = np.multiply( mcWeights[7] , 43.0 ) 
	mcWeights = [ np.hstack((mcWeights[0],mcWeights[1],mcWeights[2],mcWeights[3])) ] + [ np.hstack((mcWeights[4],mcWeights[5],mcWeights[6],mcWeights[7])) ]
	labels = ["Photon QCD" , "tt+y(y)" ] 
	nMCsamples = len(labels) 

	nClusters = 7
	nNeighbors = 25

	### Perform the Optimization ###

	makePlots = True   	
	dataClustering , dataRooHist , ArgList_Ratios , fractionRooHists , fractionHists = predictClusterTemplateComposition( 
		data , monteCarlo , mcWeights , nClusters , nNeighbors , labels , allBranches , makePlots )
	
	#getClusterTemplateSystematics(
		
	### Print the Results ###

	print( "----- FOUND COMPOSITION ESTIMATE ----- " )
	print( "MC Samples Accepted: " )
	print( "\t", labels )
	for iSample in range(len(monteCarlo)): 
		print( "\t# Monte Carlo events from MC Sample " , iSample , " = " , len(monteCarlo[iSample]) )
	print( "Prediction: " )
	for iSample in range(nMCsamples):
		print( ArgList_Ratios[iSample].getValV() , " +/- " , ArgList_Ratios[iSample].getError() )  #ratios_fullSystErrors[iSample] ) 
	print( "   " ) 
	print( "   " ) 
 
# 	### Make Plots of Various Distributions by Cluster ### 
# 
# 	plotRange = [0,1]
# 	variableHistos = [] 
# 	variableCanvases = []
# 	clusterTexts = []
# 
# 	#plt_vars = [ 'mT' , 'n_jets25_central' , 'n_bjets25_70' , 'y1_topoetcone20/y1_pt' , 'y2_topoetcone20/y2_pt' , 'y1_pt/m_yy' , 'y2_pt/m_yy' ]
# 	binning = [ [ 20, 0, 100 ] , [ 12, 0, 12 ] , [ 12, 0, 12 ] , 
# 				[ 80, -4, 4 ] , [ 80, -4, 4  ] , [ 30 , 0 , 3 ] , [ 30 , 0 , 3 ] , 
# 				[ 7, 0, 7 ] , [ 7, 0, 7 ] , [ 7, 0, 7 ], [ 7, 0, 7 ], [ 7, 0, 7 ] , [ 7, 0, 7 ] , [ 7, 0, 7 ] ]
# 
# 	for iVar, var in enumerate( branches2read + jet_branches2read ):
# 		hists_to_plot = [] 
# 		clusterTexts = []
# 		for iCluster in range(nClusters): 
# 
# 			x = allData[:,iVar+2][dataClustering.labels_[:]==iCluster]
# 			w = np.ones(len(x)) # * ( 1.0 / len(allData[:,iVar+2]) )
# 		
# 			hName = var + "_cluster" + str(iCluster) 
# 			variableHistos.append( ROOT.TH1D(hName,hName,binning[iVar][0],binning[iVar][1],binning[iVar][2]) )
# 			variableHistos[-1].FillN( len(x) , x , w , 1 )
# 			hists_to_plot.append( variableHistos[-1] )
# 			if( hists_to_plot[-1].Integral() != 0 ): hists_to_plot[-1].Scale( 1.0 / hists_to_plot[-1].Integral() )
# 			clusterTexts.append( "Cluster " + str(iCluster) )
# 
# 		varcanvas, hists_to_plot, legend = DrawNiceHistograms.DrawNiceHistograms( 
# 				histograms = hists_to_plot, 
# 				legend_labels = clusterTexts,
# 				ATLASstyle = True,
# 				drawopt = "hist", 
# 				linewidth = 2, 
# 				markersize = 0, 
# 				xtitle = var, 
# 				ytitle = "Events / Bin" , 
# 				yrange = [0.0 , 1.1] ,
# 				xrange = binning[iVar],
# 				title = "2016 Data, " + var )
# 	
# 		variableCanvases.append( varcanvas )
# 		varcanvas.Print("varCanv_"+var+".png")
# 	
# 
# 	### Draw M_yy ###
# 
# 	data_myy = readInputFiles( in_Data_FileNames , selection2apply , branches2read , jet_branches2read )[0:1]
# 	data_myy = data_myy[0]
# 	data_myy = data_myy[0]
# 	data_myy = data_myy[:10000,2]
# 
# 	hists = [] 
# 	for iCluster in range(nClusters): 
# 		hName = "myy_"+ str(iCluster)
# 		hists.append( ROOT.TH1F(hName,hName,100,100,200) )
# 		x = data_myy[dataClustering.labels_[:]==iCluster]
# 		w = np.ones(len(x))
# 		hists[-1].FillN( len(x) , x , w , 1 ) 
# 		clusterTexts.append( "Cluster " + str(iCluster) )
# 
# 	varcanvas, hists_to_plot, legend = DrawNiceHistograms.DrawNiceHistograms( 
# 			histograms = hists, 
# 			legend_labels = clusterTexts,
# 			ATLASstyle = True,
# 			drawopt = "hist", 
# 			linewidth = 2, 
# 			markersize = 0, 
# 			yrange = [-9999 , -9999] ,
# 			xrange = [-9999 , -9999] ,
# 			xtitle = "M_{yy} [GeV]", 
# 			ytitle = "Events / Bin" , 
# 			title = "Clustered 2016 Data, M_{yy}" )
# 	
# 	### Draw Correlations ### 
# 
# 	corrCanvas = ROOT.TCanvas("corrCanvas","corrCanvas",600,600)
# 	corrHists = []
# 	correlations = []
# 	nVars = len( branches2read + jet_branches2read )
# 	correlationHistogram = ROOT.TH1D("correlationHistogram","correlationHistogram",nVars,-0.5,nVars-0.5 )
# 	for iVar, var in enumerate( branches2read + jet_branches2read ): 
# 		hName = var + "_clusterCorr"
# 		corrHists.append( ROOT.TH2D(hName,hName,binning[iVar][0],binning[iVar][1],binning[iVar][2],nClusters,-0.5,nClusters-0.5 ) )
# 		for iCluster in range(nClusters): 
# 			x = allData[:,iVar+2][dataClustering.labels_[:]==iCluster]
# 			w = np.ones(len(x)) # * ( 1.0 / len(allData[:,iVar+2]) )
# 			y = np.multiply( w , iCluster ) 
# 			corrHists[-1].FillN( len(x) , x , y, w , 1 )
# 		correlationHistogram.Fill( iVar , corrHists[-1].GetCorrelationFactor() )
# 
# 	correlationHistogram.Draw() 
# 	for iVar, var in enumerate( branches2read + jet_branches2read ): 
# 		correlationHistogram.GetXaxis().SetBinLabel(iVar+1,var) 
# 		
# 
# 	corrCanvas, hists_to_plot = DrawNiceHistograms.DrawNiceHistograms( 
# 			histograms = [correlationHistogram], 
# 			ATLASstyle = True,
# 			drawopt = "p", 
# 			linewidth = 0, 
# 			markersize = 1, 
# 			xtitle = "Variable", 
# 			ytitle = "Correlation Factor" , 
# 			yrange = [-1.1 , 1.1] ,
# 			title = "Variable Correlations with Cluster" )

	